function default_loading(target, active = true) {
  $target = $(target);
  if (active) {
    $target.addClass("ajax-loading");
    $target.append(
      "<div class='loading-container'><img src='/arquivos/ajax-loader.gif' class='loading' /></div>"
    );
  } else {
    $target.removeClass("ajax-loading");
    $target.find(".loading").remove();
  }
}

default_loading($("#product-page .plugin-preco"), true);

function customizarOculos() {
  var vlr1 = "";
  var vlr2 = "";
  var vlr3 = "";

  var vlrSku = $(
    ".product-details .price-box .descricao-preco .valor-por strong"
  ).text();
  $(".custom-oculos .custom-footer .custom-valor span").text(vlrSku);

  $(".custom-oculos").fadeIn();
  $("#overlay").fadeIn();

  $("p.prazo").each(function () {
    var servicosTitulos = $(this).text().split(" + ");
    if (servicosTitulos.length) {
      for (var i = 0; i < servicosTitulos.length; i++) {
        var titulo = servicosTitulos[i];
        $("#lente-passo1 .custom-tipolente li label strong").each(function () {
          var titleOption = $(this).text();
          if (titleOption == titulo || titleOption == "NÃ£o queroa armaÃ§Ã£o") {
            $(this).parent().parent().addClass("disponivel");
          }
        });
      }
    }
  });

  $("fieldset.tipos-de-lentes").on("change", function () {
    $(".custom-oculos .custom-footer .custom-valor span").text(vlrSku);

    $("#lente-passo2").fadeOut();
    $("#lente-passo3").fadeOut();

    var vlr2 = "";
    var vlr3 = "";

    $(
      "#lente-passo2 .custom-tipolente li input, #lente-passo3 .custom-tipolente li input"
    ).each(function () {
      $(this).prop("checked", false);
    });

    vlr1 = $(this).find("input:checked").val();

    $("#lente-passo2 .custom-tipolente li").removeClass("disponivel");

    $("fieldset input").next().find("span.valorServico").remove();

    if (vlr1 && vlr1 != "") {
      if (vlr1 == "none") {
        actionButton1(vlr1);

        $("#custom-passo2 .custom-body .custom-tipolente li input").each(
          function () {
            if ($(this).val().indexOf("Lente") == -1) {
              $(this).parent().addClass("disponivel");
              $(
                '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
              ).prop("disabled", true);
            }
          }
        );
      } else {
        $(this).addClass("ativo");
        $("fieldset.espessura-de-lentes").removeClass("ativo");

        $("#lente-passo1 fieldset").removeClass("aguardando");
        $("#lente-passo2 fieldset").addClass("aguardando");
        $("#lente-passo3 fieldset").addClass("aguardando");

        $("p.prazo").each(function () {
          var $this = $(this);
          var servicosTitulos = $this.text().split(" + ");

          if (servicosTitulos.length) {
            $("#lente-passo2 .custom-tipolente li label strong").each(
              function () {
                var titleOption = $(this).text();

                if (vlr1 == servicosTitulos[0]) {
                  if (titleOption == servicosTitulos[1]) {
                    $(this).parent().parent().addClass("disponivel");
                  }

                  if (servicosTitulos.length == "2") {
                    if (titleOption == servicosTitulos[1]) {
                      var valorServico = $this.next().next().text();
                      valorServico = valorServico.replace("Valor Total: ", "");

                      if (valorServico) {
                        if (!$(this).next("span.valorServico").length) {
                          $(this).after(
                            $(
                              '<span class="valorServico">+ ' +
                                valorServico +
                                "</span>"
                            )
                          );
                        }
                      }
                    }
                  }
                }
              }
            );
          }
        });

        $("#custom-passo2 .custom-body .custom-tipolente li input").each(
          function () {
            $(this).parent().addClass("disponivel");
            $(
              '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
            ).prop("disabled", true);
          }
        );
      }
    }

    var ac = 0;
    $("#lente-passo2 .custom-tipolente li.disponivel").each(function () {
      ac++;
    });

    if (ac) {
      $("#lente-passo2").fadeIn();
      $("fieldset.espessura-de-lentes").addClass("ativo");
    }
  });

  $("fieldset.espessura-de-lentes").on("change", function () {
    $("#lente-passo3").fadeOut();

    vlr2 = $(this).find("input:checked").val();
    var vlr3 = "";

    $("#lente-passo3 .custom-tipolente li input").each(function () {
      $(this).prop("checked", false);
    });

    $("#lente-passo3 .custom-tipolente li").removeClass("disponivel");

    if (vlr2 && vlr2 != "") {
      $("#lente-passo2 fieldset").removeClass("aguardando");
      $("#lente-passo3 fieldset").addClass("aguardando");

      $(this).addClass("ativo");
      $("fieldset.escolha-de-lentes").removeClass("ativo");

      $("p.prazo").each(function () {
        var $this = $(this);

        var servicosTitulos = $this.text().split(" + ");

        if (servicosTitulos.length) {
          $("#lente-passo3 .custom-tipolente li label strong").each(
            function () {
              var titleOption = $(this).text();

              if (vlr1 == servicosTitulos[0] && vlr2 == servicosTitulos[1]) {
                if (titleOption == servicosTitulos[2]) {
                  $(this).parent().parent().addClass("disponivel");
                }

                if (servicosTitulos.length == "3") {
                  if (titleOption == servicosTitulos[2]) {
                    var valorServico = $this.next().next().text();
                    valorServico = valorServico.replace("Valor Total: ", "");

                    if (valorServico) {
                      if (!$(this).next("span.valorServico").length) {
                        $(this).after(
                          $(
                            '<span class="valorServico">+ ' +
                              valorServico +
                              "</span>"
                          )
                        );
                      }
                    }
                  }
                }
              }
            }
          );
        }
      });

      if (
        $("fieldset.espessura-de-lentes input:checked")
          .next()
          .find(".valorServico").length
      ) {
        var vlrServ = $("fieldset.espessura-de-lentes input:checked")
          .next()
          .find(".valorServico")
          .text();
        // var vlrProd = $('#custom-passo1 .custom-footer .custom-valor span').text();
        var total = customSoma(vlrServ, vlrSku);
        $(".custom-oculos .custom-footer .custom-valor span").text(
          "R$ " + total
        );
      }
    }

    var ac = 0;
    $("#lente-passo3 .custom-tipolente li.disponivel").each(function () {
      ac++;
    });

    if (ac) {
      $("#lente-passo3").fadeIn();
      $("fieldset.escolha-de-lentes").addClass("ativo");
    } else {
      $("#lente-passo3 fieldset").removeClass("aguardando");

      var fieldsetAtivo = "0";

      $("fieldset.ativo").each(function () {
        if (!$(this).hasClass("aguardando")) {
          fieldsetAtivo++;
        } else {
          fieldsetAtivo = "0";
        }
      });

      if (fieldsetAtivo > "0") {
        eventosClick1("espessura-de-lentes");
      } else {
        alert("Selecione uma opÃ§Ã£o vÃ¡lida");
        $("#custom-passo1 .custom-footer .custom-btn a").unbind("click");
      }
    }
  });

  $("fieldset.escolha-de-lentes").on("change", function () {
    vlr3 = $(this).find("input:checked").val();

    if (vlr3 && vlr3 != "") {
      $("#lente-passo3 fieldset").removeClass("aguardando");

      var fieldsetAtivo = "0";

      $("fieldset.ativo").each(function () {
        if (!$(this).hasClass("aguardando")) {
          fieldsetAtivo++;
        } else {
          fieldsetAtivo = "0";
        }
      });

      if (fieldsetAtivo > "0") {
        eventosClick1("escolha-de-lentes");
      } else {
        alert("Selecione uma opÃ§Ã£o vÃ¡lida");
      }
    }

    if (
      $("fieldset.escolha-de-lentes input:checked").next().find(".valorServico")
        .length
    ) {
      var vlrServ = $("fieldset.escolha-de-lentes input:checked")
        .next()
        .find(".valorServico")
        .text();
      // var vlrProd = $('#custom-passo1 .custom-footer .custom-valor span').text();
      var total = customSoma(vlrServ, vlrSku);
      $(".custom-oculos .custom-footer .custom-valor span").text("R$ " + total);
    }
  });
}

function eventosClick1(fieldsetClass) {
  var combinacao = "";
  var offeringId = "";
  var valorServico = "";

  $("li.disponivel input:checked").each(function () {
    if (combinacao) {
      combinacao = combinacao + " + " + $(this).val();
    } else {
      combinacao = $(this).val();
    }
  });

  $("p.prazo").each(function () {
    var servicosTitulos = $(this).text().indexOf(combinacao);
    if (servicosTitulos != -1) {
      var href = $(this).next().next().next().find("a").attr("href");

      if (href.indexOf("service=") != -1) {
        offeringId = href.split("service=")[1].split("&")[0];
      }
    }
  });

  actionButton1(offeringId);
}

function actionButton1(offeringId) {
  $("#custom-passo1 .custom-footer .custom-btn a").on("click", function () {
    var y = 0;

    $("fieldset.ativo").each(function () {
      var vlrIn = $(this).find("input:checked").val();
      if (typeof vlrIn == "undefined") {
        y = 0;
      } else {
        y++;
      }
    });

    if (!$("fieldset.tipos-de-lentes").hasClass("ativo")) {
      var gravacao = $("fieldset.tipos-de-lentes input:checked").val();
      if (gravacao == "none") {
        y = "1";
      }
    }

    if (y) {
      if (offeringId) {
        $("#custom-passo1").addClass("custom-loading");

        var itens = [];
        var prod = {};
        var href = $(".buy-button.buy-button-ref").attr("href");
        var idProdSku = href.split("sku=")[1].split("&")[0];
        var itemIndex = "";

        prod.id = idProdSku;
        prod.quantity = href.split("qty=")[1].split("&")[0];
        prod.seller = href.split("seller=")[1].split("&")[0];

        itens.push(prod);

        vtexjs.checkout.getOrderForm().then(function (orderForm) {
          vtexjs.checkout.addToCart(itens, null).done(function (orderForm) {
            for (var i = 0; i < orderForm.items.length; i++) {
              if (idProdSku == orderForm.items[i].id) {
                itemIndex = i;

                if (offeringId == "none") {
                  eventosClick2(itemIndex);
                } else {
                  vtexjs.checkout
                    .getOrderForm()
                    .then(function () {
                      return vtexjs.checkout.addOffering(offeringId, itemIndex);
                    })
                    .done(function (orderForm) {
                      eventosClick2(itemIndex);
                    });
                }
              }
            }
          });
        });
      }
    } else {
      console.log($("fieldset.gravacao").find("input:checked").val());
    }
  });
}

function eventosClick2(itemIndex) {
  $("#custom-passo1").fadeOut("fast");
  $("#custom-passo2").fadeIn();

  var localGravacao = "";
  var txtGravacao = "";

  $("#custom-passo2 fieldset.gravacao").on("change", function () {
    localGravacao = $(this).find("input:checked").val();
    if (localGravacao && localGravacao != "none") {
      $(
        "#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input"
      ).prop("disabled", false);

      $("#custom-passo2 fieldset").removeClass("aguardando");
    } else if (localGravacao == "none") {
      $(
        '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
      )
        .val("")
        .trigger("change");
      $(
        '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
      ).prop("disabled", true);
      $(
        '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
      ).css("border-color", "#d1d1d1");
    }
  });

  $("#custom-passo2 .custom-footer .custom-btn a").on("click", function () {
    if (localGravacao && localGravacao != "none") {
      var txtGravacao = $(
        '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
      ).val();

      if (txtGravacao && txtGravacao != "") {
        $("#custom-passo2").addClass("custom-loading");

        var attachmentName = "gravacao";

        var content = {
          Local: localGravacao,
          Texto: txtGravacao,
        };

        vtexjs.checkout
          .addItemAttachment(itemIndex, attachmentName, content)
          .done(function (orderForm) {
            eventosClick3(itemIndex);
          });
      } else {
        $(
          '#custom-passo2 .custom-body .custom-lenteprop .custom-lentetext input[name="gravatexto"]'
        ).css("border-color", "red");
      }
    } else if (localGravacao == "none") {
      eventosClick3(itemIndex);
    }
  });
}

function eventosClick3(itemIndex) {
  $("#custom-passo2").fadeOut("fast");
  $("#custom-passo3").fadeIn();
  $(
    '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"], #custom-passo3 .custom-lentetext textarea[name="mensagem"]'
  ).prop("disabled", true);

  var embalar = "";
  var remetente = "";
  var destinatario = "";
  var mensagem = "";
  var offeringId = "";

  $("p.prazo").each(function () {
    var servicosTitulos = $(this).text().indexOf("embalar");
    if (servicosTitulos != -1) {
      var vlrServ = $(this).next().next().text();
      vlrServ = vlrServ.replace("Valor Total: R$ ", "");

      if (
        !$(
          '#custom-passo3 .custom-tipolente li label[for="embalagem-sim"] strong span.valorServico'
        ).length
      ) {
        $(
          '#custom-passo3 .custom-tipolente li label[for="embalagem-sim"] strong'
        ).append('<span class="valorServico">R$ ' + vlrServ + "</span>");
      }
    }
  });

  $("#custom-passo3 fieldset.embalagem-presente").on("change", function () {
    embalar = $(this).find("input:checked").val();

    $("#custom-passo3 .custom-footer .custom-valor span").text(
      $("#custom-passo2 .custom-footer .custom-valor span").text()
    );

    if (embalar && embalar != "none") {
      var vlrServ = $(
        '#custom-passo3 .custom-tipolente li label[for="embalagem-sim"] strong span.valorServico'
      ).text();

      var totalFinal = $(
        "#custom-passo3 .custom-footer .custom-valor span"
      ).text();
      var valorAtualizado = customSoma(vlrServ, totalFinal);

      $("#custom-passo3 .custom-footer .custom-valor span").text(
        "R$ " + valorAtualizado
      );

      $(
        '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"], #custom-passo3 .custom-lentetext textarea[name="mensagem"]'
      ).prop("disabled", false);

      $("#custom-passo2 fieldset").removeClass("aguardando");

      $("p.prazo").each(function () {
        var servicosTitulos = $(this).text().indexOf(embalar);
        if (servicosTitulos != -1) {
          var href = $(this).next().next().next().find("a").attr("href");

          if (href.indexOf("service=") != -1) {
            offeringId = href.split("service=")[1].split("&")[0];
          }
        }
      });
    } else if (embalar == "none") {
      $(
        '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"], #custom-passo3 .custom-lentetext textarea[name="mensagem"]'
      )
        .val("")
        .trigger("change");
      $(
        '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"], #custom-passo3 .custom-lentetext textarea[name="mensagem"]'
      ).prop("disabled", true);
      $(
        '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"], #custom-passo3 .custom-lentetext textarea[name="mensagem"]'
      ).css("border-color", "#d1d1d1");
    }
  });

  $("#custom-passo3 .custom-footer .custom-btn a").on("click", function () {
    if (embalar && embalar != "none") {
      var remetente = $(
        '#custom-passo3 .custom-lentetext input[name="remetente"]'
      ).val();
      var destinatario = $(
        '#custom-passo3 .custom-lentetext input[name="destinatario"]'
      ).val();
      var mensagem = $(
        '#custom-passo3 .custom-lentetext textarea[name="mensagem"]'
      ).val();

      if (remetente && destinatario) {
        var attachmentName = "embalar-para-presente";

        $("#custom-passo3").addClass("custom-loading");

        var content = {
          De: remetente,
          Para: destinatario,
          Mensagem: mensagem,
        };

        vtexjs.checkout
          .addOffering(offeringId, itemIndex)
          .done(function (orderForm) {
            vtexjs.checkout
              .addBundleItemAttachment(
                itemIndex,
                offeringId,
                attachmentName,
                content
              )
              .done(function (orderForm) {
                window.location.href = "/checkout/#/cart";
              });
          });
      } else {
        $(
          '#custom-passo3 .custom-lentetext input[name="remetente"], #custom-passo3 .custom-lentetext input[name="destinatario"]'
        ).css("border-color", "red");
      }
    } else if (embalar == "none") {
      window.location.href = "/checkout/#/cart";
    }
  });
}

function customSoma(valorS, valorP) {
  var valorDoServico = valorS;
  valorDoServico = valorDoServico.split("R$ ")[1];
  valorDoServico = valorDoServico.replace(".", "");
  valorDoServico = valorDoServico.replace(",", ".");
  valorDoServico = parseFloat(valorDoServico);

  var valorDoProduto = valorP;
  valorDoProduto = valorDoProduto.split("R$ ")[1];
  valorDoProduto = valorDoProduto.replace(".", "");
  valorDoProduto = valorDoProduto.replace(",", ".");
  valorDoProduto = parseFloat(valorDoProduto);

  var vlrCustom = valorDoServico + valorDoProduto;
  vlrCustom = vlrCustom.toFixed(2);
  vlrCustom = vlrCustom.replace(".", ",");

  return vlrCustom;
}

function ordenacaoTr() {
  var ordem = [
    "espec_0",
    "espec_1",
    "espec_2",
    "espec_3",
    "espec_4",
    "espec_5",
    "espec_6",
  ];
  var ordemReal = [];

  for (var i = 0; i < ordem.length; i++) {
    var posicao = ordem[i];
    var trAtivo = $(
      ".produto-lentes .product-info .receita-infos tr[data-ordem='" +
        posicao +
        "']"
    );

    if (trAtivo.length > 0) {
      ordemReal.push(posicao);
    }
  }

  for (var i = 1; i < ordemReal.length; i++) {
    var posicaoAtual = ordemReal[i];
    var posicaoAnterior = ordemReal[i - 1];

    var trAtual = $(
      ".produto-lentes .product-info .receita-infos tr[data-ordem='" +
        posicaoAtual +
        "']"
    );
    var trAnterior = $(
      ".produto-lentes .product-info .receita-infos tr[data-ordem='" +
        posicaoAnterior +
        "']"
    );

    trAtual.insertAfter(trAnterior);
  }
}

function camposReceita(ladoOlho) {
  $(
    '.produto-lentes .product-info .receita-infos td[data-label="' +
      ladoOlho +
      '"] select:not(#quantidadeLente)'
  ).remove();

  $(".product-info .seletor-sku #" + ladoOlho + " .select .sku-selector").each(
    function () {
      var specification = $(this).attr("specification");
      var ordem = $(this).attr("name");
      $(
        '.produto-lentes .product-info .receita-infos tr[data-espeficacao="' +
          specification +
          '"] td[data-label="' +
          ladoOlho +
          '"]'
      ).append($(this).clone());
      $(
        '.produto-lentes .product-info .receita-infos tr[data-espeficacao="' +
          specification +
          '"]'
      ).attr("data-ordem", ordem);
      $(
        '.produto-lentes .product-info .receita-infos tr[data-espeficacao="' +
          specification +
          '"]'
      ).removeClass("oculta-tr");
    }
  );

  $(
    '.produto-lentes .product-info .receita-infos td[data-label="' +
      ladoOlho +
      '"] select option'
  ).each(function () {
    if ($(this).prop("disabled")) {
      $(this).remove();
    }
  });

  ordenacaoTr();
  atualizaCampos(ladoOlho);
}

function atualizaCampos(ladoOlho) {
  $(
    '.produto-lentes .product-info .receita-infos td[data-label="' +
      ladoOlho +
      '"] select'
  ).on("change", function () {
    var olhoChecked = $(
      ".product-details .seletor-sku fieldset input:checked"
    ).val();
    var idSel = $(this).attr("id");
    var vlrSel = $(this).val();

    if (olhoChecked == "olhodif") {
      $(
        "body#product-page .product-details .seletor-sku #" +
          ladoOlho +
          " .sku-selector-container"
      )
        .find("#" + idSel)
        .val(vlrSel)
        .trigger("change");
      camposReceita(ladoOlho);
    } else {
      $(this).parent().next().find("select").val(vlrSel).trigger("change");
      var vlrDif = $(this).parent().next().find("select").val();

      if (vlrSel != vlrDif) {
        $(this).parent().next().find("select").val("").trigger("change");
      }

      $(
        "body#product-page .product-details .seletor-sku #" +
          ladoOlho +
          " .sku-selector-container"
      )
        .find("#" + idSel)
        .val(vlrSel)
        .trigger("change");
      camposReceita(ladoOlho);
    }
  });
}

function modoReceita() {
  $(".product-details .seletor-sku fieldset").on("change", function () {
    var olhoChecked = $(this).find("input:checked").val();

    if (olhoChecked == "olhodif") {
      $(".produto-lentes .product-info .receita-infos td").each(function () {
        $(this).removeClass("desativado");
      });
      $("tr.olhoUnico").removeClass("ativo");
      $("tr.olhoUnico").addClass("desativado");
      $("tr.olhoDiferente").removeClass("desativado");
      $("tr.olhoDiferente").addClass("ativo");
    } else {
      $(
        '.produto-lentes .product-info .receita-infos td[data-label*="direito"] select'
      ).each(function () {
        var selVlrAt = $(this).val();
        if (selVlrAt) {
          $(this)
            .parent()
            .next()
            .find("select")
            .val(selVlrAt)
            .trigger("change");
          var vlrDif = $(this).parent().next().find("select").val();
          if (selVlrAt != vlrDif) {
            $(this).parent().next().find("select").val("").trigger("change");
          }
        }
      });

      $(".produto-lentes .product-info .receita-infos td").each(function () {
        var dataLabel = $(this).data("label");
        if (dataLabel && dataLabel.indexOf("esquerdo") != -1) {
          $(this).addClass("desativado");
        }
      });

      $("tr.olhoUnico").addClass("ativo");
      $("tr.olhoUnico").removeClass("desativado");
      $("tr.olhoDiferente").addClass("desativado");
      $("tr.olhoDiferente").removeClass("ativo");
    }
  });
}

function funcaoComprarLentedeContato() {
  var empty = false;

  $(".product-info .seletor-sku .select .sku-selector").each(function () {
    if ($(this).val() == "") {
      empty = true;
    }
  });

  console.log(empty);

  if (!empty) {
    var $btnComprarProduto = $(".buy-button.buy-button-ref");

    var skuOD = [];
    var i = 0;
    $(
      '.receita-infos tr:not(.oculta-tr) td[data-label="direito"] select:not(#quantidadeLente)'
    ).each(function () {
      var data = $(this).val();
      if (data) {
        data = data.toString();
        skuOD[i] = data;
        i++;
      } else {
        $(this).addClass("empty");
      }
    });

    var skuOE = [];
    var i = 0;
    $(
      '.receita-infos tr:not(.oculta-tr) td[data-label="esquerdo"] select:not(#quantidadeLente)'
    ).each(function () {
      var data = $(this).val();
      if (data) {
        data = data.toString();
        skuOE[i] = data;
        i++;
      } else {
        $(this).addClass("empty");
      }
    });

    var skuIdDireito = "";

    for (var x = 0; x < skuJson.skus.length; x++) {
      var valoresSku = skuJson.skus[x].values.toString();
      skuOD = skuOD.toString();

      if (valoresSku == skuOD) {
        if (skuJson.skus[x].available) {
          skuIdDireito = skuJson.skus[x].sku;
        } else {
          console.log(skuJson.skus[x].available);
        }
      }
    }

    var skuIdEsquerdo = "";

    for (var x = 0; x < skuJson.skus.length; x++) {
      var valoresSku = skuJson.skus[x].values.toString();
      skuOE = skuOE.toString();

      if (valoresSku == skuOE) {
        if (skuJson.skus[x].available) {
          skuIdEsquerdo = skuJson.skus[x].sku;
        } else {
          console.log(skuJson.skus[x].available);
        }
      }
    }

    var qtdDir = $(
      '.receita-infos td[data-label="direito"] select#quantidadeLente'
    ).val();
    var qtdEsq = $(
      '.receita-infos td[data-label="esquerdo"] select#quantidadeLente'
    ).val();

    if (skuIdDireito && skuIdEsquerdo) {
      var hrefBuy =
        "/checkout/cart/add?sku=" +
        skuIdDireito +
        "&qty=" +
        qtdDir +
        "&seller=1&sku=" +
        skuIdEsquerdo +
        "&qty=" +
        qtdEsq +
        "&seller=1&redirect=true&sc=1";
      window.location.href = hrefBuy;
    } else {
      alert("CombinaÃ§Ã£o indisponÃ­vel no momento");
    }
  } else {
    alert("Preencha todos os campos");
  }
}

function receitaLenteDeContato() {
  modoReceita();
  camposReceita("esquerdo");
  camposReceita("direito");

  $("input#olhodif").click();

  $(".produto-lentes .product-info .receita-infos select").each(function () {
    var $optionFirst = $(this).find("option").first();

    if ($optionFirst.val() == "") $optionFirst.text("SELECIONE");
  });

  $(
    '.produto-lentes select[data-dimension="Diametro"], .produto-lentes select[data-dimension="Curva Base"]'
  ).each(function () {
    var qtdOptions = $(this).find('option[class^="skuopcao"]').length;

    if (qtdOptions > 1) {
      $(this).addClass("selecione-item");
    }
  });
}

function dimensoesArmacao() {
  var ocTamanho = $("td.value-field.Tamanho").text();
  var ocAro = $("td.value-field.Aro").text();
  var ocPonte = $("td.value-field.Ponte").text();
  var ocHaste = $("td.value-field.Haste").text();
  var ocFrontal = $("td.value-field.Frontal").text();
  var ocAltura = $("td.value-field.Altura").text();

  ocTamanho && $("span.oc-tamanho").text(ocTamanho);
  ocAro && $("span.oc-aro").text(ocAro);
  ocPonte && $("span.oc-ponte").text(ocPonte);
  ocHaste && $("span.oc-haste").text(ocHaste);
  ocFrontal && $("span.oc-frontal").text(ocFrontal);
  ocAltura && $("span.oc-altura").text(ocAltura);

  var dimensoesFrente = "";
  var dimensoesLado = "";

  if (ocAro) {
    dimensoesFrente += '<span class="oc-aro">' + ocAro + "</span>";
  }

  if (ocPonte) {
    dimensoesFrente += '<span class="oc-ponte">' + ocPonte + "</span>";
  }
  if (ocFrontal) {
    dimensoesFrente += '<span class="oc-frontal">' + ocFrontal + "</span>";
  }
  if (ocAltura) {
    dimensoesFrente += '<span class="oc-altura">' + ocAltura + "</span>";
  }
  if (ocHaste) {
    dimensoesLado = '<span class="oc-haste ">' + ocHaste + "</span>";
  }
  if (dimensoesFrente) {
    $("#dimensoes-frente .box-banner").prepend(dimensoesFrente);
  }
  if (dimensoesLado) {
    $("#dimensoes-lado .box-banner").prepend(dimensoesLado);
  }
}

function searchResults(response) {
  var idResultados = [""];

  if (
    !$(".search.search-top .showSearchResults .prateleiras-resultados").length
  ) {
    $(".search.search-top .showSearchResults").append(
      '<div class="prateleiras-resultados"><div id="loadingSearch"></div></div>'
    );
  } else {
    $(".search.search-top .showSearchResults .prateleiras-resultados").remove();
    $(".search.search-top .showSearchResults").append(
      '<div class="prateleiras-resultados"><div id="loadingSearch"></div></div>'
    );
  }

  for (var i = 0; i < response.length; i++) {
    idResultados[i] = response[i].productId;
  }

  var qtdProds = idResultados.length;

  var idsProdutos = idResultados
    .filter(function (id) {
      return $.trim(id);
    })
    .map(function (id) {
      return "fq=productId:" + id;
    })
    .join("&");

  var idPrateleira = "ef3fcb99-de72-4251-aa57-71fe5b6e149f";

  if (idsProdutos) {
    $.ajax(
      "/buscapagina?" +
        idsProdutos +
        "&PS=" +
        qtdProds +
        "&sl=" +
        idPrateleira +
        "&cc=100&sm=0&PageNumber=1",
      {
        async: !1,
      }
    )
      .done(function (data) {
        $(".search.search-top .showSearchResults .prateleiras-resultados").html(
          data
        );
      })
      .fail(function () {
        $(".search.search-top .showSearchResults .prateleiras-resultados").html(
          "<h4>Ops, nÃ£o encontramos nada em sua busca</h4>"
        );
      });
  } else {
    $(".search.search-top .showSearchResults .prateleiras-resultados").html(
      "<h4>Ops, nÃ£o encontramos nada em sua busca</h4>"
    );
  }

  var $showCaseOwl = $(
    ".search.search-top .showSearchResults .prateleiras-resultados .prateleira.vitrine ul"
  );

  if ($showCaseOwl.length) {
    $showCaseOwl.find(".helperComplement").remove();
    $showCaseOwl.owlCarousel({
      items: 4,
      autoPlay: false,
      stopOnHover: true,
      pagination: false,
      itemsDesktop: [1298, 4],
      itemsDesktopSmall: [980, 3],
      itemsTablet: [768, 3],
      itemsMobile: [479, 2],
      navigation: true,
      navigationText: [
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
      ],
    });
  }
}

/* PRECISA SER REFATORADO */
function imagensPrateleira() {
  $(".prateleira .box-item").each(function () {
    var $this = $(this);
    var prodUrl = $this.find("a.product-image").attr("href");
    if (prodUrl.indexOf(".br") != -1) {
      prodUrl = prodUrl.split(".br")[1];
      var settings = {
        async: true,
        crossDomain: true,
        url: "/api/catalog_system/pub/products/search" + prodUrl,
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      };
      $.ajax(settings).done(function (response) {
        if (
          response[0].items[0].images.length > 1 &&
          !$this.find("img.img2").length
        ) {
          var imageTag = response[0].items[0].images[1].imageTag;
          $this.find("a.product-image img").addClass("img1");
          imageTag = imageTag.replace(
            'src="~/',
            'src="https://showdeotica.vteximg.com.br/'
          );
          imageTag = imageTag.replace(/\#width#/g, "400");
          imageTag = imageTag.replace(/\#height#/g, "220");
          imageTag = imageTag.replace("<img src=", "<img class='img2' src=");
          $this.find("a.product-image").append(imageTag);
        }
      });
    }
  });
}

imagensPrateleira();

/* REMOVE A IMAGEM SE NÃƒO EXISTIR */
function imgError(img) {
  img.error(function () {
    $(this).remove();
  });
}

// OrdenaÃ§Ã£o na sidebar
function filtroMobile() {
  var widthScreen = $(window).width();

  if (widthScreen <= 767) {
    if ($(".sidebar fieldset.orderBy").length == 0) {
      $(".sidebar").append($(".main .sub:last-child").clone());
    }
  }
}

function precoBoletoBuyBox() {
  if (typeof skuJson_0 != "undefined" && skuJson_0.available == true) {
    var items = [
      {
        id: skuJson_0.skus[0].sku,
        quantity: 1,
        seller: skuJson_0.skus[0].sellerId,
      },
    ];

    vtexjs.checkout.simulateShipping(items).done(function (result) {
      var response = result;

      var data = "";

      if (result && result.ratesAndBenefitsData) {
        data = result.ratesAndBenefitsData.teaser;
      }

      if (data.length) {
        for (var i = 0; i < data.length; i++) {
          var nome = data[i].name;
          var valor = data[i].effects.parameters[0].value;
          var tipo = data[i].effects.parameters[0].name;

          var paymentMethod = data[i].conditions.parameters[0].name;
          var paymentMethodId = data[i].conditions.parameters[0].value;

          var precoPor = $(
            ".product-details .price-box .descricao-preco .valor-por strong"
          )
            .text()
            .replace("R$ ", "");
          precoPor = precoPor.replace(".", "");
          precoPor = precoPor.replace(",", ".");

          if (tipo == "PercentualDiscount" && valor > 0) {
            if (
              (paymentMethod == "PaymentMethodId" &&
                paymentMethodId.indexOf("6") !== -1) ||
              paymentMethodId.indexOf("201") !== -1
            ) {
              var calcular =
                parseFloat(precoPor) -
                (parseFloat(precoPor) * parseFloat(valor)) / 100;
              calcular = calcular.toFixed(2).split(".");
              calcular[0] =
                "R$ " + calcular[0].split(/(?=(?:...)*$)/).join(".");
              calcular = calcular.join(",");

              $("body#product-page .prod-boleto").html(
                '<span class="skuPrice"><strong>' +
                  calcular +
                  "</strong> Ã€  vista no boleto bancÃ¡rio</span>"
              );
              $("body#product-page .prod-boleto").show();

              return true;
            }
          }
        }
      }
    });
  } else {
    $("body").addClass("indisponivel");
  }
}

function paymentMethodsPopUp() {
  var skuProd = skuJson_0.skus[0].sku;

  $(function () {
    $.ajax({
      url:
        "https://www.showdeotica.com.br/productotherpaymentsystems/" + skuProd,
      dataType: "html",
    }).done(function (data) {
      var dataContent = $(data);

      $("#product-page #formasPagto").html(dataContent.find(".content").html());
      $("body").append(
        '<link href="//showdeotica.vteximg.com.br/css/vtex.commerce.otherPaymentSystems.css" rel="stylesheet" type="text/css" />'
      );
      $("body").append(
        '<script src="//showdeotica.vteximg.com.br/Scripts/vtex.commerce.productOtherPaymentSystems.js" type="text/javascript"></script>'
      );

      $("div#formasPagto div#divBoleto").remove();
    });
  });
}

function creatNewSkuSelector() {
  $(".product-image .seletor-sku").append(
    '<div class="boxSkuSelector"><ul class="subOptions"></ul></div>'
  );

  if (
    typeof $(".product-image .seletor-sku .select .sku-selector").val() !=
    "undefined"
  ) {
    if (
      $(".product-image .seletor-sku .select .sku-selector").val() != "N/A" &&
      $(".product-image .seletor-sku .select .sku-selector").val() != ""
    ) {
      var vlrSelected = $(
        ".product-image .seletor-sku .select .sku-selector"
      ).val();
    } else {
      $(".product-details .product-info .boxSkuSelector").hide();
    }
  } else {
    $(".product-details .product-info .boxSkuSelector").hide();
  }

  for (var i = 0; i < skuJson.skus.length; i++) {
    if (skuJson.skus[i].values != "N/A") {
      if (skuJson.skus[i].values != "") {
        $(".boxSkuSelector .subOptions").append(
          '<li id="' +
            skuJson.skus[i].values +
            '" class="itemSkuSelector">' +
            '<div class="imgSku">' +
            '<img src="' +
            skuJson.skus[i].image +
            '" alt="' +
            skuJson.skus[i].values +
            '">' +
            "</div>" +
            '<div class="variationDescription">' +
            "<p>" +
            skuJson.skus[i].values +
            "</p>" +
            "</div>" +
            "</li>"
        );
      }
    }
  }

  $(".subOptions .itemSkuSelector").click(function () {
    var id = $(this).attr("id");

    $(".subOptions .itemSkuSelector").removeClass("itemSkuSelected");
    $(this).addClass("itemSkuSelected");

    $(".product-image .seletor-sku .select .sku-selector")
      .val(id)
      .trigger("change");
  });

  $(".product-image .seletor-sku .select .sku-selector").on(
    "change",
    function () {
      var selected = $(this).val();

      $(this)
        .children("option")
        .each(function () {
          if ($(this).val() == selected) {
            if ($(this).hasClass("item_unavaliable")) {
              $("body").addClass("indisponivel");
            } else {
              $("body").removeClass("indisponivel");
            }
          }
        });

      $(".apresentacao .thumbs").data("owlCarousel").destroy();
      carouselThumbs();
    }
  );
}

function checkUserLoggedIn() {
  var user_info;

  $.ajax({
    type: "GET",
    url: "/no-cache/profileSystem/getProfile",
    success: function (data) {
      user_info = data;

      if (user_info.IsUserDefined) {
        var name = "";

        if (user_info.FirstName) {
          name = user_info.FirstName;
        } else if (user_info.LastName) {
          name = user_info.FirstName;
        } else {
          name = user_info.Email.split("@")[0];
        }

        $("li.head-loginsepara").hide();
        $("li.head-loginentrar a").text("Sair");
        $(".header .sing-in a span.visible-lg.visible-md").text(
          "OlÃƒÆ’Ã‚Â¡, " + name
        );
        $("li.head-loginentrar a").attr("href", "/no-cache/user/logout");
        $(".head-loginitens").addClass("logado");
      }
    },
  });
}

function showPercentDiscountShelf() {
  $(".prateleira .hasBestPrice").each(function () {
    if (!$(this).hasClass("priceModified")) {
      var precoPor = parseFloat(
        $(this)
          .find(".price .best-price")
          .text()
          .replace("R$ ", "")
          .replace(".", "")
          .replace(",", ".")
      );

      var precoDe = parseFloat(
        $(this)
          .find(".price .old-price")
          .text()
          .replace("R$ ", "")
          .replace(".", "")
          .replace(",", ".")
      );

      var porcentagem = (((precoDe - precoPor) / precoDe) * 100).toFixed(2);
      porcentagem = Math.floor(porcentagem);

      $(this)
        .find(".product-image .desc")
        .append('<p class="flag seloBestPrice">' + porcentagem + "% OFF</p>");

      $(this).addClass("priceModified");
    }
  });
}

function showPercentDiscountProductPage() {
  if (!$("body").hasClass("indisponivel")) {
    if (
      $(".product-details .price-box .descricao-preco .valor-por strong").html()
    ) {
      if (
        $(
          ".product-details .price-box .descricao-preco .valor-de strong"
        ).html()
      ) {
        var precoPor = parseFloat(
          $(".product-details .price-box .descricao-preco .valor-por strong")
            .text()
            .replace("R$ ", "")
            .replace(".", "")
            .replace(",", ".")
        );

        var precoDe = parseFloat(
          $(".product-details .price-box .descricao-preco .valor-de strong")
            .text()
            .replace("R$ ", "")
            .replace(".", "")
            .replace(",", ".")
        );

        var porcentagem = (((precoDe - precoPor) / precoDe) * 100).toFixed(2);
        porcentagem = Math.floor(porcentagem);

        $(".product-image .flags").append(
          '<p class="flag seloBestPrice">' + porcentagem + "% OFF</p>"
        );
      }
    }
  }
}

function layoutCompreJunto() {
  $(".product-description .buy-together-content tr").each(function () {
    var $this = $(this);

    if (typeof $this.attr("class")) {
      var nrParcelasCj = $this.find("td.buy strong:nth-child(1)").text();
      var vlrParcelaCj = $this.find("td.buy strong:nth-child(2)").text();
      var vlrTotalCj = $this.find("td.buy").text();
      vlrTotalCj = vlrTotalCj.split("total: ")[1].split(" Compra")[0];

      $this
        .find("td.buy p.comprar-junto")
        .before(
          '<p class="precoTotal"><span class="pTotal">Compre os 2 itens por:</span><strong>' +
            vlrTotalCj +
            '</strong><span class="pParcelado"><strong>' +
            nrParcelasCj +
            "</strong> de <strong>" +
            vlrParcelaCj +
            "</strong> s/juros</span> </p>"
        );
    }
  });
}

function filterOptionsLimiter() {
  var limiter = 10;
  $("#departament-navegador .menu-departamento ul").each(function () {
    var j = 0;
    $(this)
      .children("li")
      .each(function () {
        j++;
        if (j > limiter) {
          $(this).addClass("vejaMais");
        }
      });
    if (j > limiter) {
      $(this).append('<li class="maisOpcoes"></li>');
      $(this).append('<li class="linkVejaMais">+ ver todas</li>');
    }
    if ($.trim($(this).html()) == "") {
      $(this).hide();
      $(this).prev("h5").hide();
    }
  });

  $("#departament-navegador .menu-departamento ul li.vejaMais").each(
    function () {
      $(this).parent().children(".maisOpcoes").append($(this));
    }
  );

  $("#departament-navegador .menu-departamento ul li.linkVejaMais").click(
    function () {
      $(this).toggleClass("ocultar");
      $(this).prev().slideToggle();
    }
  );
}

function carouselThumbs() {
  var $showCaseOwl = $(".apresentacao .thumbs");

  if ($showCaseOwl.length) {
    $showCaseOwl.find(".helperComplement").remove();
    $showCaseOwl.owlCarousel({
      items: 4,
      autoPlay: false,
      stopOnHover: true,
      pagination: false,
      itemsDesktop: [1298, 4],
      itemsDesktopSmall: [980, 4],
      itemsTablet: [768, 4],
      itemsMobile: [479, 4],
      navigation: true,
      navigationText: [
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
      ],
    });
  }
}

function variationsSkus() {
  $(".prateleira .box-item").each(function () {
    var $this = $(this);
    var data = $this.data("id");
    var infoSkus = "";

    if (typeof data != "undefined") {
      var settings = {
        async: true,
        crossDomain: true,
        url: "/api/catalog_system/pub/products/crossselling/similars/" + data,
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      };

      $.ajax(settings).done(function (response) {
        if (response.length) {
          infoSkus = response;

          $this.find(".product-mini").append('<div class="imageSkus"></div>');

          for (var i = 0; i < infoSkus.length; i++) {
            var categoria = infoSkus[i].categories.join(",");

            if (categoria != -1) {
              var link = infoSkus[i].link;
              var productName = infoSkus[i].productName;
              var skuName = infoSkus[i].items[0].name;
              var skuImg = infoSkus[i].items[0].images[0].imageUrl;

              if (i < 3) {
                if (skuImg.length) {
                  $this
                    .find(".imageSkus")
                    .append(
                      '<span><a href="' +
                        link +
                        '" class="' +
                        skuName +
                        '"><img src="' +
                        skuImg +
                        '" alt="' +
                        productName +
                        '"></a></span>'
                    );
                }
              }
            }
          }
        }
      });
    }
  });
}

function fixedHeader() {
  var windowWidth = $(window).width();
  var headerHeigth = $(".header-container").height();

  if (windowWidth > 767) {
    if (scrollY >= headerHeigth) {
      $(".header-container").addClass("fixed");
      headerHeigth = $(".header-container.fixed").height();
      $("body").css("padding-top", headerHeigth + "px");
      $(".search.search-top").css("top", headerHeigth + "px");
    } else {
      $(".header-container").removeClass("fixed");
      headerHeigth = $(".header-container").height();
      $("body").css("padding-top", "0px");
      $(".search.search-top").css(
        "top",
        headerHeigth - $(window).scrollTop() + "px"
      );
    }
  } else {
    if ($(".header-container").hasClass("fixed")) {
      $(".header-container").removeClass("fixed");
    }
  }
}

$(window).on("scroll", function () {
  fixedHeader();
});

$(document).ready(function () {
  if ($.fn.ADMAKEadvancedFilter) {
    $(document).ADMAKEadvancedFilter({
      tipoFiltros: {},
    });
  }
  if ($.fn.ADMAKEmenu) {
    $(document).ADMAKEmenu();
  }
  $(".col-mini-cart").ADMAKEminiCart({
    miniCartQtd: ".mini-cart-qty-admake",
  });

  var $btnComprar = $(".btn-add-buy-button-asynchronous");
  if ($btnComprar.length) {
    $btnComprar.html('adicionar ao carrinho <i class="fa fa-lock"></i>');
  }

  var $btnComprarProduto = $(".buy-button.buy-button-ref");
  if ($btnComprarProduto.length) {
    if ($("#comprar-flutuante").length) {
      var $comprarFlutuante = $("#comprar-flutuante");
      var btnComprarTop = $(".product-info .buy-button-box").offset().top;
      $(window).scroll(function () {
        if ($(window).width() > 768) {
          if (
            $(this).scrollTop() >= btnComprarTop &&
            !$comprarFlutuante.is(":visible")
          ) {
            $comprarFlutuante.fadeIn(function () {
              var urlImage =
                $("#include #image-main").attr("src") != ""
                  ? $("#include #image-main").attr("src")
                  : "/arquivos/sem-foto.gif";
              $("#foto-comprar-flutuante").attr("src", urlImage);
              $("body").css("padding-bottom", $comprarFlutuante.height() + 30);
            });
          } else if (
            $(this).scrollTop() < btnComprarTop &&
            $comprarFlutuante.is(":visible")
          ) {
            $comprarFlutuante.fadeOut(function () {
              $("body").css("padding-bottom", 0);
            });
          }
        }
      });
    }

    $btnComprarProduto.html("adicionar ao carrinho");
  }

  if ($.fn.owlCarousel) {
    var $fullbanner = $(".fullbanner");
    if ($fullbanner.length) {
      $fullbanner.owlCarousel({
        items: 1,
        singleItem: true,
        autoPlay: true,
        stopOnHover: true,
        navigation: true,
        autoHeight: false,
        navigationText: [
          '<i class="fa fa-chevron-left"></i>',
          '<i class="fa fa-chevron-right"></i>',
        ],
      });
    }

    var $showCaseOwl = $("div#prodSimilar .sku-similar ul");
    if ($showCaseOwl.length) {
      $showCaseOwl.find(".helperComplement").remove();
      $showCaseOwl.owlCarousel({
        items: 5,
        autoPlay: false,
        pagination: false,
        itemsDesktop: [1199, 5],
        itemsDesktopSmall: [980, 4],
        itemsTablet: [768, 3],
        itemsMobile: [479, 3],
        navigation: false,
      });
    }

    var $showCaseOwl = $(".home-marcas .marcas");
    if ($showCaseOwl.length) {
      $showCaseOwl.find(".helperComplement").remove();
      $showCaseOwl.owlCarousel({
        items: 7,
        autoPlay: true,
        stopOnHover: true,
        pagination: true,
        itemsDesktop: [1280, 7],
        itemsDesktopSmall: [980, 6],
        itemsTablet: [768, 5],
        itemsMobile: [479, 3],
        navigation: true,
        navigationText: [
          '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
          '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
        ],
      });
    }

    var $showCaseOwl = $(".showcase-owl .prateleira > ul");
    if ($showCaseOwl.length) {
      $showCaseOwl.find(".helperComplement").remove();
      $showCaseOwl.owlCarousel({
        items: 4,
        autoPlay: false,
        stopOnHover: true,
        pagination: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [980, 4],
        itemsTablet: [768, 3],
        itemsMobile: [479, 1],
        navigation: true,
        navigationText: [
          '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
          '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
        ],
      });
    }
  }

  /* menu mobile filtro */
  $("#departament-navegador .depart-title").click(function () {
    if ($("body").hasClass("versao-mobile")) {
      $(this).toggleClass("active");
      $(".menu-departamento").slideToggle("medium");
    }
  });

  if (typeof skuJson_0 != "undefined") {
    // paymentMethodsPopUp();
    precoBoletoBuyBox();
    creatNewSkuSelector();
    showPercentDiscountProductPage();
    carouselProduct();
    actionButton1();

    if ($("body").hasClass("produto-lentes")) {
      $("#skuName").text(skuJson.name);

      // receitaLenteDeContato();
      var $btnComprarProduto = $(".buy-button.buy-button-ref");

      $btnComprarProduto.live("click", function () {
        comprarLentedeContato();
        console.log("clicou aqui");
      });
      if (vtxctx && vtxctx.departmentName == "Lentes de Contato Coloridas") {
        $("input#olhoigual").click();
        $(".receita-infos").addClass("lentes-coloridas");
      }
    } else {
      if ($("body#product-page .skuService").length) {
        $(".buy-button-box .glass-custom").css("display", "block");

        $(".buy-button.buy-button-ref").on("click", function (e) {
          var src = $(this).attr("href");
          if (src.indexOf("sku") != -1) {
            e.preventDefault();
            customizarOculos();
          }
        });

        $(".glass-custom").on("click", function () {
          var src = $(".buy-button.buy-button-ref").attr("href");
          if (src.indexOf("sku") != -1) {
            customizarOculos();
          }
        });
      }

      dimensoesArmacao();

      if ($("div#prodSimilar").html()) {
        $("div#prodSimilar .box-item").each(function () {
          var $this = $(this);
          var idProd = $this.data("id");

          if (typeof idProd != "undefined") {
            vtexjs.catalog
              .getProductWithVariations(idProd)
              .done(function (product) {
                var skuName = product.skus[0].skuname;
                $this.find("b.product-name a").text(skuName);
              });
          }
        });
      } else {
        $(".product-details .seletor-sku .title").hide();
      }
    }
  }

  checkUserLoggedIn();
  showPercentDiscountShelf();

  var compreJunto = $(".product-description .buy-together-content").html();

  if (compreJunto) {
    layoutCompreJunto();
  } else {
    console.log("este produto nÃ£o tem compre junto");
  }

  if ($("#departament-navegador .menu-departamento ul").html()) {
    filterOptionsLimiter();
  }

  var brandName = $("body#brand-page #box-bread-brumb li.last a").text();

  if (brandName) {
    $(".title-brand h2").text(brandName);
  }

  carouselThumbs();
  variationsSkus();

  /*
	if( $('body').hasClass('home') ) {
		$('div#popupHome .popupNews').prepend('<span class="fecharPopUp"><i class="fa fa-times-circle" aria-hidden="true"></i></span>');
		if( typeof Cookies.get('newsletter') == "undefined" ) {
			if( $(window).width() > 767 ){
				$('div#popupHome').fadeIn();
				$('div#popupHome').removeClass('hide');
				Cookies.set('newsletter', '1', { expires: 3 });
			}
		}
	}

	$('div#popupHome .overlaypopup, div#popupHome .popupNews span.fecharPopUp').click(function(){
		$('div#popupHome').fadeOut();
		$('div#popupHome').addClass('hide');
	});
	*/

  filtroMobile();

  // BUSCA SHOW DE Ã“TICA

  $("#busca-header").on("keyup", function () {
    var busca = $(this).val();

    var settings = {
      async: true,
      crossDomain: true,
      url: "/api/catalog_system/pub/products/search/" + busca,
      method: "GET",
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    };

    $.ajax(settings).done(function (response) {
      searchResults(response);
    });
  });

  //imagensPrateleira();
});

$(window).on("load", function () {
  $(".recebe-menu").append($("#top-menu .container").clone());

  /* seta no menu principal */
  $("#top-menu ul.menu li, #menu-mobile ul.menu li").each(function () {
    var $this = $(this);
    if ($this.children(".menu-level2").length) {
      $(this).addClass("seta");
    }
  });

  $("#menu-mobile ul li.seta").live("click", function () {
    $(this).find(".menu-level2").slideToggle();
    $(this).toggleClass("open");
  });

  $("#menu-mobile ul li.menu-super-ofertas").live("click", function () {
    $(this).find(".menu-superOferta").slideToggle();
  });

  if (typeof dataSuperOferta != "undefined") {
    $(".countdown").each(function () {
      $(this).countdown(dataSuperOferta, function (event) {
        $(this).html(
          event.strftime(
            "<ul><li><span>%D</span><span>Dias</span></li><li><span>%H</span><span>Horas</span></li><li><span>%M</span><span>Min</span></li><li><span>%S</span><span>Seg</span></li></ul>"
          )
        );
      });
    });
  }

  $(window).on("resize", function () {
    filtroMobile();

    if ($(".desktop-header-container .headerMenu nav ul.menu").length > 1 ){
      $(".desktop-header-container .headerMenu nav ul.menu")[1].remove()
    }


  });

  // altera o nome do input buttom
  var $btnfrete = $(".product-info .shipping-box .freight-btn");
  if ($btnfrete.length) {
    $btnfrete.val("calcular");
  }
});

$("#overlay, .conteudoPopUp .fechar, .custom-oculos .fechar").click(
  function () {
    $("#overlay").fadeOut();
    $(".conteudoPopUp").fadeOut();
    $(".custom-oculos").fadeOut();
  }
);

$(".pgto").click(function () {
  $("#overlay").fadeIn();
  $(".conteudoPopUp").fadeIn();
  $("#formasPagto").fadeIn();
});

if ($("a.see-other-payment-method-link").length) {
  var urlFormaPagto = $("a.see-other-payment-method-link")
    .attr("onclick")
    .split("'")[1];

  $("#formasPagto").append(
    '<iframe src="' +
      urlFormaPagto +
      '" width="100%" height="100%" border="0"></iframe>'
  );

  $(".see-other-payment-method-link").remove();
}

/* coloca span no texto do tÃ­tulo */
$(
  ".product-description .product-description-box h4, .product-description .product-description-box .title, .product-description .product-description-box #divTitulo, .showcase-default h2, h2.home-titulo, #institucional-content .titulo-institucional.title-404"
).each(function () {
  var txt = $(this).text();
  $(this).html("<span>" + txt + "</span>");
});

/* menu mobile footer */
$(
  ".footer .col-inst-duvidas .menu-select, .footer .col-atendimento-ajuda .menu-select, .footer .col-pagamento .menu-select, .footer .col-atendimento-social .menu-select, .menu-select"
).click(function () {
  if ($(window).width() < 768) {
    $(this).toggleClass("hover");
    $(this).find("ul").slideToggle("medium");
  }
});

/* produto informaÃ§Ãµes */
$(".product-description-box h2").click(function () {
  if ($(window).width() < 768) {
    $(this).toggleClass("hover");
    $(this).next().slideToggle("medium");
  }
});

/* header busca */
$(".header .search-btn, .search.search-top span.fechar, .blackBoxSearch").click(
  function () {
    var headerHeigth = $(".header-container").height();

    $(this).toggleClass("hover");
    $(".search.search-top").slideToggle("medium");
    $(".blackBoxSearch").fadeToggle();

    if ($(".header-container").hasClass("fixed")) {
      $(".search.search-top").css("top", headerHeigth + "px");
    } else {
      $(".search.search-top").css(
        "top",
        headerHeigth - $(window).scrollTop() + "px"
      );
    }
  }
);

/* filtros departamento */
$("#departament-navegador .menu-departamento h5").click(function () {
  if ($(window).width() > 767) {
    $(this).toggleClass("hover");
    $(this).next("ul").slideToggle("medium");
  }
});

// FORMULÃRIO CENTRAL DE ATENDIMENTO

function postMasterDataContact(name, email, fields) {
  var store = "showdeotica";
  var urlProtocol = window.location.protocol;
  var apiUrl =
    urlProtocol + "/" + store + "/dataentities/" + name + "/documents";
  var response;

  var who = {
    email: email,
  };

  var data = $.extend(who, fields);

  $.ajax({
    headers: {
      Accept: "application/vnd.vtex.ds.v10+json",
      "Content-Type": "application/json",
    },
    url: apiUrl,
    async: true,
    crossDomain: true,
    type: "POST",
    data: JSON.stringify(data),
  })
    .success(function (data) {
      response = data;
      $("div#result h3").text("Seus dados foram enviados com sucesso!");
      $("div#result").fadeIn("slow");
      $("div#result").addClass("sucess");
      $("div#result").removeClass("fail");
    })
    .fail(function (data) {
      response = data;
      $("div#result h3").text(
        "Ocorreu um erro ao enviar seus dados, por favor, tente novamente mais tarde."
      );
      $("div#result").fadeIn("slow");
      $("div#result").addClass("fail");
      $("div#result").removeClass("sucess");
    });
  return response;
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function setFields() {
  var clientData = {
    page: $('input[name="page"]').val(),
    name: $('input[name="nome"]').val(),
    email: $('input[name="endereco_email"]').val(),
    subject: $('input[name="assunto"]').val(),
    message: $("textarea#mensagem").val(),
  };

  if (validateEmail(clientData.email)) {
    postMasterDataContact("CO", clientData.email, clientData);
  } else {
    alert("Insira um e-mail vÃ¡lido");
  }
}

$("button#enviarMP").click(function () {
  if ($('input[name="page"]').val() == "Atendimento") {
    if (
      $('input[name="nome"]').val() == "" ||
      $('input[name="endereco_email"]').val() == "" ||
      $('input[name="assunto"]').val() == "" ||
      $("textarea#mensagem").val() == ""
    ) {
      alert("Preencha todos os campos");
    } else {
      setFields();
    }
  }
});

$('.ajax-content-loader[rel="/no-cache/user/welcome"]').on(
  "DOMSubtreeModified",
  function () {
    var welcome = $("p.welcome em a").text();
    var welcomeMSg = welcome.indexOf("Novo UsuÃ¡rio?");

    console.log(welcomeMSg);

    if (welcomeMSg == -1) {
      $(
        ".header .sing-in a.header-login, .header .sing-in .head-loginitens ul li.head-loginentrar a, .header .sing-in .head-loginitens ul li.head-loginsepara a"
      ).attr("id", "login");
      $(
        ".header .sing-in a.header-login, .header .sing-in .head-loginitens ul li.head-loginentrar a, .header .sing-in .head-loginitens ul li.head-loginsepara a"
      ).removeAttr("href");
    } else {
      $(".header .sing-in .head-loginitens ul li.head-loginentrar a").attr(
        "href",
        "/no-cache/user/logout"
      );
    }
  }
);

$(document).ajaxSuccess(function (event, xhr, settings) {
  if (settings.url.indexOf("buscapagina") !== -1) {
    imagensPrateleira();
    variationsSkus();
  }
});

// Produto imagem principal com scroll
function carouselProduct() {
  var productImages = [];

  $(".product-image .thumbs li a").each(function () {
    var self = $(this);

    productImages.push({
      image: self.attr("rel"),
      imageBg: self.attr("zoom"),
      thumb: self.find("img").attr("src"),
    });
  });

  $("#product-content").prev(".product-images-slider").remove();
  $(".product-images-slider.text-center.desktop").remove();

  // $('.product-name-mobile .product-name').before('<div class="product-images-slider text-center"></div>');
  // $('#product-content').before('<div class="product-images-slider text-center visible-xs"></div>');
  $("#include").before(
    '<div class="product-images-slider text-center desktop"></div>'
  );

  for (var i = 0; i < productImages.length; i++) {
    console.log("productImages ", productImages);
    if (productImages[i].image) {
      if (productImages[i].imageBg) {
        $(".product-images-slider").append(
          '<a class="image-zoom" href="' +
            productImages[i].imageBg +
            '"><img src="' +
            productImages[i].image +
            '" /></a>'
        );
      } else {
        $(".product-images-slider").append(
          '<img src="' + productImages[i].image + '" />'
        );
      }
    }
  }

  var $productImagesSlider = $(".product-images-slider.text-center.desktop");
  if ($productImagesSlider.length) {
    $productImagesSlider.owlCarousel({
      items: 1,
      pagination: true,
      singleItem: true,
      autoPlay: false,
      stopOnHover: true,
      navigation: true,
      autoHeight: true,
      navigationText: [
        '<i class="fa fa-chevron-left"></i>',
        '<i class="fa fa-chevron-right"></i>',
      ],
    });
  }
}

// FUNÃ‡ÃƒO NEWSLETTER
function cadastraNewsletter(name, email, fields) {
  var store = "showdeotica";
  var urlProtocol = window.location.protocol;
  var apiUrl =
    urlProtocol + "/" + store + "/dataentities/" + name + "/documents";
  var response;

  var who = {
    email: email,
  };

  var data = $.extend(who, fields);

  $.ajax({
    headers: {
      Accept: "application/vnd.vtex.ds.v10+json",
      "Content-Type": "application/json",
    },
    url: apiUrl,
    async: true,
    crossDomain: true,
    type: "PUT",
    data: JSON.stringify(data),
  })
    .success(function (data) {
      response = data;
      $("p.newsSucess").show();
    })
    .fail(function (data) {
      response = data;
      $("p.newsSucess").text(
        "Erro ao cadastrar newsletter, por favor, tente novamente."
      );
      $("p.newsSucess").show();
    });
  return response;
}

$("#newsletterClientEmailDesktop").on("click", function () {
  $("p.newsSucess").hide();
  $("p.newsError").hide();
});

$("input.newsletter-button-ok").on("click", function () {
  var infoNews = {
    newsCateg: $(this).val(),
    email: $(this).parent().find("input.newsletter-client-email").val(),
    isNewsletterOptIn: true,
  };

  $("p.newsError").hide();

  if (validateEmail(infoNews.email)) {
    cadastraNewsletter("CL", infoNews.email, infoNews);
  } else {
    $("p.newsError").show();
  }
});

//AvaliaÃ§Ã£o Produto
$(document).ajaxSuccess(function (event, xhr, settings) {
  if (settings.url.indexOf("userreview") !== -1) {
    var $spanRating = $("#spnRatingProdutoBottom");

    $(".avalicao-media-nota").remove();

    if ($spanRating.length) {
      var ratingAttrClass = $spanRating.attr("class");

      if (ratingAttrClass.indexOf(" ") != -1) {
        var ratingSplit = ratingAttrClass.split(" ");
        var ratingClassAval = ratingSplit[1].replace("avaliacao", "");
        var avaliacao = insereVirgula(ratingClassAval);
      } else if (ratingAttrClass.indexOf("avaliacao") != -1) {
        var ratingClassAval = ratingAttrClass.replace("avaliacao", "");
        var avaliacao = insereVirgula(ratingClassAval);
      }

      console.log(ratingClassAval);
      console.log(typeof ratingClassAval);

      $spanRating.before(
        '<span class="avalicao-media-nota">' + avaliacao + "</span>"
      );
    }

    function insereVirgula(val) {
      if (val != "0") {
        return val.substr(0, 1) + "," + val.substr(1, 1);
      } else if (val == "0") {
        return "0,0";
      }
    }
  }
});
$(document).ready(function () {
  setTimeout(function(){
    addNewSkuColors()
  },500)
  if (vtxctx && vtxctx.departmentName == "Lentes de Contato Coloridas") {
    $("input#olhoigual").click();
    $(".receita-infos").addClass("lentes-coloridas");
  }

  //Adiciona a quantidade em cima do minicart 
  try {
    $(".headerMinicart")[0].append($(".badge.bg-danger.mini-cart-qty-admake")[1]) 
    $(".desktop-header-container #mini-cart-admake").html("")
    $(".desktop-header-container #mini-cart-admake").parent().append($(".mobile-header-container #mini-cart-admake"))
  }catch (e) {

  }

 if ($("body").hasClass("account") || $("body").hasClass("login") )
    $("title").html("Minha conta"); 
});

/* correcao carrosseis */
$(window).load(function () {
  $(".owl-carousel").each(function () {
    // pega o elemento do carrossel
    var owl = $(this);

    // pega a instancia do carrossel conforme elemento
    var owlInstance = owl.data("owlCarousel");

    // se a instancia existir
    if (owlInstance != null) {
      $(window).resize(function () {
        owlInstance.reinit();
      });
    }
  });


/*Carrossel banners abaixo do fullbanner na HOME*/
  $("body#home-page .banners-2").owlCarousel({
    loop: !1,
    items: 5,
    nav: true,
    dots: true,
    autoPlay: true,
    itemsMobile: [479, 2],
    navigation: true,
    navigationText: [
      '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
      '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
    ],
  })


  
  if ($("body").attr("id") === "home-page") {
    $(".tooltipHeader").remove();

    $(".helperComplement").remove();

    $("body.home-page .prateleira > ul").owlCarousel({
      items: 6,
      itemsMobile: [479, 2],
      loop: true,
      dots: false,
      navigation: true,
      navigationText: [
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
      ],
      autoPlay: true,
    })

/*    $("body.home-page .prateleira .n4colunas > ul").owlCarousel({
      items: 4,
      itemsMobile: [479, 2],
      loop: true,
      dots: false,
      navigation: true,
      navigationText: [
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
        '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
      ],
      autoPlay: true,
    })

    */
  
  $(".trigger-wrapper").owlCarousel({
    items:4 ,
    itemsMobile: [768, 2],
    itemsMobile: [479,1],
  })
   
  
  }


$(".banners-abaixo-frete").owlCarousel({
    loop: true,
    items: 1,
    dots: false,
    navigation: false,
    autoPlay: true,
    navigationText: [
      '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-left"></i></button>',
      '<button class="btn btn-default btn-lg"><i class="fa fa-chevron-right"></i></button>',
    ],
  })
  
//Clicar no primeiro SKU disponível tela Produto
if ($("body").hasClass("produto") || $("body").hasClass("product")) {
      if (sessionStorage.getItem("product-sku-url") !== null)  {
       let urlToClick = sessionStorage.getItem("product-sku-url");
       try {
        urlToClick = urlToClick.replace("http", "https")
        $("img[src='"+urlToClick+"']").parent().click()
      }catch(e) {
         console.warn(e.message)
       }
       setTimeout(function(){
         sessionStorage.removeItem("product-sku-url")
        }, 5000)

      }else {
        for (let i = 0 ; i < skuJson_0.skus.length; i++){
          if (skuJson_0.skus[i].available) {
          //console.log(skuJson_0.skus[i].values[0])     
            try{
         $("[id='"+ skuJson_0.skus[i].values[0] + "']").click()
            }catch(e) {
              console.log(e.message);
            }
          break;
         }
       }        
      }


    $("label[for=olhoigual]").html("Quero apenas 1 grau")
    $(".avaliacoes").remove
 
if ($("body").hasClass("produto-lente")){
 var olhoIgualCheckBox = document.getElementById("olhoigual");
  olhoIgualCheckBox.addEventListener('change', function(){
    setTimeout(function(){
      if ($(olhoIgualCheckBox).next("div").hasClass("ativo")) {
        $("#caixas-combo-container select option[value=2]").attr("selected", "selected")
      }else {
        $("#caixas-combo-container select option[value=1]").attr("selected", "selected")
      }
    }, 500)
    })
 }
}
//Remove as dimensões se o produto não for óculos
  if (vtxctx.departmentyId !== "4" && vtxctx.departmentyId !==  "1") {
    // $("#product-extra-description-2").next(".product-description-box").remove() 
    $("#formatorosto").remove() 
    //console.log("Não é óculos")
 }
});

$(".prateleira .box-item").on('click', function() {
  let skuUrl = $(this).find("img").attr("src");
  sessionStorage.setItem("product-sku-url", skuUrl);
})

var classname ;
var elemento ;
var jqueryElemento ;
var eachSku = [] ;
var coresSkusVitrine ;

$(window).load(function(){
  // Vitrine (categoria)
if ( $("body").attr("id") === "category-page" || $("body").attr("id") === "brand-page" || $("body").attr("id") === "departament-page") {

$("body.product .produtos-relacionados ul").addClass('owl-carousel')
$("body.product .produtos-relacionados ul").owlCarousel({
  items: 4,
  nav: true,
  dots: true,
})
//importa o axios e faz o request da simulação pra cada item da vitrine
$.getScript("https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.js").then(() => {
 $(".vitrine .prateleira ul li .box-item").each((pos,el) => {
  skuId = $(el).attr("sku-id")
  if (skuId !== undefined) {
    $(el).find(".best-price").html("<img src='/arquivos/ajax-loader.gif' width='20' height='20'/>")
    $(el).find(".installment").remove();

    axios.post("/api/checkout/pub/orderForms/simulation", {
    items:[
      {
          "id": skuId,
          "quantity": "1",
          "seller": "1"
      }],
    "paymentData": {
          "id": "paymentData",
          "payments" : [{
          "paymentSystem": "1",
          "installments": "1"
            }
          ],
    },
    "country": "BRA"
    }).then(res => {
      //A resposta da simulação mostra o valor do preço a vista no cartão de crédito
    let precoProdutoAVista = parseInt(res.data.totals[0].value) / 100
    let precoCartaoaVista = parseInt(res.data.paymentData.installmentOptions[1].installments[0].total) / 100;
    let quantidadeParcelas;
    let valorParcela;
    for (let i = 0; i < res.data.paymentData.installmentOptions[0].installments.length; i++ ) {
      quantidadeParcelas = res.data.paymentData.installmentOptions[0].installments[i].count;
      valorParcela = res.data.paymentData.installmentOptions[0].installments[i].value / 100 
    }
    $(el).find(".old-price").html("<span style='text-decoration: none;'>De: </span>" + $(el).find(".old-price").html());
    $(el).find(".best-price").html("<p>Por R$ "+ formatarBrl(precoProdutoAVista) +"</p>" + "<small style='color: #000; font-size: 12px; line-height: 0.5; font-weight: lighter; position: relative; top: -35px;'><br>ou em até " + quantidadeParcelas+"x " + "R$ " + valorParcela  +" s/juros </small> <br> <div class='a-vista-cartao-credito-wrapper' style='position: relative; top: -30px;'> <span class='a-vista-cartao-credito-currency' style='color: #006fba; font-weight: bold;' > R$ " + formatarBrl(precoCartaoaVista) + "</span><small style='font-weight: normal;'> à vista no cartão de crédito <small></div>")
    })
}else {
  console.log("SKUID INVALIDO")
    }
  })
})
}
function formatarBrl(valor) {
   return valor.toLocaleString("pt-BR", {
    type: "currency",
    format: "BRL",
    maximumFractionDigits: 2,
    minimumFractionDigits: 2
})
}
})

$(".newsletter-button-ok").on('click', function() {
  let email = $(".newsletter-client-email").text
  let nome = $(".newsletter-client-email").text
  cadastraNewsletter(email, nome, null)
})


/*fim correcao carrosseis*/
$(".product-image .seletor-sku .select .sku-selector").live(
  "change",
  function () {
    carouselProduct();
  }
);

$(document).ready(function () {
  ajustaPrecoAVista();
});

$(window).on("skuSelected", function (data) {
  console.warn("Trocou de sku...");
  ajustaPrecoAVista();
});

function ajustaPrecoAVista() {
  if ($("body").attr("id") === "product-page") {
    vtexjs.checkout.getOrderForm().done(function (orderForm) {
      var precoBoleto = 0,
        precoVisa = 0;

      function getPrices() {
        var promises = [];
        var items = [];
        var cep = "08773490";
        var sku = null;

        if (orderForm.shippingData) {
          if (orderForm.shippingData["address"]) {
            var cepRgex = /[0-9]{5}(-?)[\d]{3}/;
            var c = orderForm.shippingData["address"]["postalCode"];
            if (c.match(cepRgex)) {
              cep = c;
              if (cep) cep = cep.replace("-", "");
            }
          }
        }

        try {
          sku = selectedToBuy[0] || skuJson_0.skus[0].sku;
        } catch (error) {
          sku = skuJson_0.skus[0].sku;
        }

        items.push({
          id: sku,
          quantity: 1,
          seller: 1,
        });

        // Cartao VISA
        promises.push(
          new Promise(function (resolve) {
            $.ajax({
              url: "/api/checkout/pub/orderforms/simulation",
              dataType: "json",
              type: "POST",
              headers: {
                accept: "application/json",
                "content-type": "application/json",
              },
              data: JSON.stringify({
                postalCode: cep,
                country: "BRA",
                items: items,
                paymentData: {
                  id: "paymentData",
                  payments: [
                    {
                      paymentSystem: "1",
                      installments: 1
                    },
                  ],
                },
              }),
              success: function (d) {
                var price = 0;
                d.paymentData.installmentOptions.forEach(function (elem) {
                  if (elem.paymentSystem === "1") price = elem.value;
                });

                resolve(price);
              },
            });
          })
        );

        // Boleto
        promises.push(
          new Promise(function (resolve) {
            $.ajax({
              url: "/api/checkout/pub/orderforms/simulation",
              dataType: "json",
              type: "POST",
              headers: {
                accept: "application/json",
                "content-type": "application/json",
              },
              data: JSON.stringify({
                postalCode: cep,
                country: "BRA",
                items: items,
                paymentData: {
                  id: "paymentData",
                  payments: [
                    {
                      paymentSystem: "1",
                    },
                  ],
                },
              }),
              success: function (d) {
                var price = 0;
                d.paymentData.installmentOptions.forEach(function (elem) {
                  if (elem.paymentSystem === "1") price = elem.value;
                });

                resolve(price);
              },
            });
          })
        );
        return Promise.all(promises);
      }

      function atualizaPrecoAVista(data) {
        var valor = data["valor"];
        var descricao = data["desc"];

        valor = parseInt(valor) / 100;
        valor = valor.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
        });
        valor = valor.replace("R$", "");

        $el = $(".plugin-preco .descricao-preco .preco-a-vista-calculado");
        if ($el.length > 0) {
        } else {
          $(".plugin-preco .descricao-preco").append(
            "<div class='preco-a-vista-calculado'><small class='preco-a-vista-calculado__moeda'>R$</small><span class='preco-a-vista-calculado__valor'>" +
              valor +
              "</span><small class='preco-a-vista-calculado__sistema'>" +
              descricao +
              "</small></div>"
          );
        }

        default_loading($("#product-page .plugin-preco"), false);
      }

      getPrices().then(function (data) {
        var info = {
          valor: 0,
          desc: "",
        };
        // se boleto tiver menor valor...
        if (parseInt(data[0]) < parseInt(data[1])) {
          info.valor = data[0];
          info.desc = " à vista no boleto.";
        } else {
          //caso contrario (visa com menor valor)
          info.valor = data[1];
          info.desc = " à vista no cartão de crédito.";
        }

        atualizaPrecoAVista(info);
      });
    });
  }
}


function addNewSkuColors() {  
  $(".product-name").append("<ul class='sku-colors-wrapper' style='display: flex; justify-content: space-evenly; max-width: 10vw; margin: 10px auto;'></ul>")
  $(".prateleira ul li .product_field_430 ul li").each((pos,el) => {
    classname = el.textContent;
    elemento = document.getElementsByClassName(classname);
    jqueryElemento = $(el);
    jqueryElemento.parents(".box-item").children(".product-mini").remove();
    eachSku = classname.split(";")
    eachSku.map((col, pos) => {
    var gradient = col.split("|")
    var gradientD ="";
    // console.log(gradient)
     if (gradient.length <= 1 ) {
      // console.log(gradientD)
      // console.log(gradient[0])
      //backround normal
      jqueryElemento.parents(".box-item").find(".sku-colors-wrapper").append("<li class='sku-color' pos='"  + pos + "'style='border: 1px solid; width: 5px; height: 5px; border-radius: 50%; padding: 8px; background: "+gradient[0]+"; cursor: pointer;'><a onclick='changeSku'></a></li>")  
      }else {
      //Gradiente
      for(var i = 0; i < gradient.length; i++) {
        if( gradient[i] === "transparent" ){
          gradientD += " " + "#e0e0e0" + "-" + "10" + "%";
        }else {
        gradientD += " " + gradient[i] + "-" + parseInt(100 / gradient.length) + "%";
        //+ "-" + parseInt(100 / gradient.length) + "%"
      }
    }
      var gradientV = gradientD.replaceAll(" ", ",")
      gradientV = gradientV.replaceAll("-", " ")
      jqueryElemento.parents(".box-item").find(".sku-colors-wrapper").append("<li class='sku-color' pos='"  + pos + "'  style='border: 1px solid;width: 5px; height: 5px; border-radius: 50%; padding: 8px; background-image: linear-gradient(to bottom "+ gradientV +"); cursor: pointer;'><a onclick='changeSku'></a></li>")  
    }
    
     })
  
   })

   $('.prateleira .sku-colors-wrapper li.sku-color').on('click', function(){
    var skuId = $(this).parents('.box-item').attr('data-id');
    var position = $(this).attr("pos")
    for (let i = 0; i < productVariations.length; i++) {
      if (productVariations[i].productId == skuId && productVariations[i].skus[position] != undefined)
       $(this).parents('.box-item').find('img').attr('src', productVariations[i].skus[position].image)}
      })  

  }